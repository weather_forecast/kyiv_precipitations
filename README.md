# Precipitations forecast in Kyiv for the next 2 hours

Main purpose of the project is to predict precipitations based on radar snapshots from the website of [Ukrainian Hydrometeorological Center](https://www.meteo.gov.ua/en/Radar)
Note: Due to the destruction of the radar, there are no current data available.
## Concept

Forecast is done in 3 steps:
1. 2 radar snapshots are taken from the site of hydrometeorolical center, first is last available, second is 0.5 ~ 2 hours old.
2. Shift between cloud systems on snapshots is calculated by finding the best correlation between them.
   As shift is caused by air movement (wind), its properties are calculated (speed and direction) as well.
3. Forecast for the next 2 hours is done with assumption that wind properties will remain the same.

## Required libraries
Ensure that you have installed latest numpy, matplotlib and pillow libraries for your Python.

Also pay attention that website will work properly only using versions of tornado 4.2 and nbconvert 5.3.1 libraries.

## How-to run the forecast
Run script main.bat from the repository

Open localhost in your browser and check the forecast

## To be added soon...
Gif animations with the demonstration of the forecast work

