# import unittest
from PIL import Image, ImageDraw
import datetime

import src.tools as tls
import src.config as cnf

class TestFunc():
#     @unittest.skip("")
    def testShift2dMap(self):
        import numpy as np
        m = np.array([[1, 0, 1, 0, 1],
                      [0, 1, 0, 1, 0],
                      [1, 0, 1, 0, 1],
                      [0, 1, 0, 1, 0]])
        m_shifted = np.array([[0, 0, 0, 0, 0],
                              [0, 1, 0, 1, 0],
                              [0, 0, 1, 0, 1],
                              [0, 1, 0, 1, 0]])
        m_test = tls.shift_binary_map(m, 1, 1)
        res = np.array_equal(m_test, m_shifted)
#         self.assertEqual(res, True, "Some errors in shift_2d_map() ocurred")
    
#     @unittest.skip("")
    def testExtractWeatherMap(self):
        import src.tools as tls
        
        dt = datetime.datetime.strptime("2016-11-11 19-29-00", "%Y-%m-%d %H-%M-%S")
#         snapshot = Image.open(tls.dt_to_snapshot(dt))
        w_map = tls.get_weather_map_for_datetime(dt)
#         snapshot.show()
    
    
    def testFindBestShiftBetweenolorMaps(self):
        w = 20
        h = 20
        image = Image.new('RGB', (w, h), (255, 255, 255))
#         pixels = image.load()
        draw = ImageDraw.Draw(image)
        for i in range(7):
            for j in range(7):
                draw.point([i, j], (255, 0, 0))
#         draw.point([2, 2], (255, 0, 0))
#         draw.point([2, 4], (255, 0, 0))
        draw.point([2, 3], (0, 255, 0))
        
        draw.point([3, 2], (0, 255, 0))
        draw.point([3, 3], (255, 255, 0))
        draw.point([3, 4], (0, 255, 0))
        
#         draw.point([4, 2], (255, 0, 0))
        draw.point([4, 3], (0, 255, 0))
#         draw.point([4, 4], (255, 0, 0))
        
        
        v_shift_set = 4
        h_shift_set = 6
        image_shifted = tls.shift_color_map(image, v_shift_set, h_shift_set)
#         image.show()
#         image_shifted.show()
        
        found_shift_tuple = \
                tls.find_best_shift_between_color_maps(image, image_shifted, 
                                                      ['save'])
        print found_shift_tuple
#         self.assertEqual(found_shift_tuple, (v_shift_set, h_shift_set), 
#                 "Some errors in find_best_shift_between_color_maps() ocurred")
    def testMakeWeatherForecast(self, start_datetime = None):
        
        import numpy as np
        if start_datetime == None:
            start_datetime = datetime.datetime.strptime(cnf.TEST_START_DATETIME, 
                                                    "%H:%M %d.%m.%Y")
        i_datetime = start_datetime
        time_forecast = np.zeros((cnf.MINUTES_IN_HOUR * cnf.FORECAST_HOURS, 3), dtype = int)
        for time_shift in range(0, cnf.FORECAST_HOURS * int(cnf.MINUTES_IN_HOUR), 10):
            weather_image = tls.get_weather_map_for_datetime(i_datetime, ["save"])
            if weather_image != None: 
                weather_map = weather_image.load()
            i_datetime += datetime.timedelta(minutes=10)
            for i in range(time_shift, time_shift + 10):
                if weather_image != None:
                    time_forecast[i] = weather_map[cnf.X_HOME, cnf.Y_HOME]
                else:
                    time_forecast[i] = cnf.WHITE_COLOR_TUPLE
        tls.convert_time_forecast_to_image(time_forecast, start_datetime, ["real"])
    
    def testMakeWeatherForecastMultiple(self):
        start_datetime = datetime.datetime.strptime(cnf.TEST_START_DATETIME, 
                                                "%H:%M %d.%m.%Y")
        i_datetime = start_datetime
        while i_datetime - start_datetime < datetime.timedelta(hours=6):
            self.testMakeWeatherForecast(i_datetime)
            i_datetime += datetime.timedelta(minutes=10)
        
if __name__ == "__main__":
    testFunc = TestFunc()
    testFunc.testMakeWeatherForecastMultiple()