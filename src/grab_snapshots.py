import time, datetime, urllib.request, os.path, shutil, ssl

def grab_snapshot(actual_snapshot_dt_str):
    url_path = "http://meteo.gov.ua/radars/Ukr_J " + actual_snapshot_dt_str + ".jpg"
    local_path = "../snapshots/Ukr_J " + actual_snapshot_dt_str + ".jpg"
    
    if not os.path.isfile(local_path):
        tls_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        with urllib.request.urlopen(url_path, context=tls_context) as response, open(local_path, 'wb') as out_file:
            shutil.copyfileobj(response, out_file)
        if os.path.getsize(local_path) < 190 * 1024:
            os.remove(local_path)
            return 0
        else:
            return 1
    else:
        return 2

if __name__ == "__main__":
    
    start_time = time.time()
    
    now_dt = datetime.datetime.utcnow()
    now_str = now_dt.strftime("%Y-%m-%d %H-%M")
    now_dt_round = datetime.datetime.strptime(now_str, "%Y-%m-%d %H-%M")
    now_minutes = int(now_str[-2:])
    
    snapshot_datetime = now_dt_round - datetime.timedelta(minutes=11 + (now_minutes % 10))
    
#     start_datetime = datetime.datetime.strptime("2016-12-14 19-19-00", 
#                                                 "%Y-%m-%d %H-%M-%S")
#     snapshot_datetime = start_datetime
    fails = 0
    while fails < 150:
        actual_snapshot_dt_str = snapshot_datetime.strftime("%Y-%m-%d %H-%M-%S")
        if grab_snapshot(actual_snapshot_dt_str) == 1:
            print(actual_snapshot_dt_str)
        else:
            fails += 1
        
        snapshot_datetime -= datetime.timedelta(minutes=10)
#         url_path = "http://meteo.gov.ua/radars/Ukr_J " + actual_snapshot_dt_str + ".jpg"
#         local_path = "../snapshots/Ukr_J " + actual_snapshot_dt_str + ".jpg"
#         if not os.path.isfile(local_path):
#             urllib.urlretrieve(url_path, local_path)
#             if os.path.getsize(local_path) < 190 * 1024:
#                 os.remove(local_path)
#                 fails += 1
#             else:
#                 print actual_snapshot_dt_str
#         else:
#             break
    
    print("Downloading time: %.1f sec" % (time.time() - start_time))