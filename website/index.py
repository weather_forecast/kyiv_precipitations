import tornado.ioloop
import tornado.web

import datetime, os.path


class BaseHandler(tornado.web.RequestHandler):
    pass


class MainHandler(BaseHandler):
    @tornado.web.asynchronous
    def get(self):
        info_file = open("website/resources/last_update_datetime.txt", "r")
        dt = info_file.readline()[:-1]
        time_points = self.get_time_points(dt)
        img_pc_name = info_file.readline()[:-1]
        img_mobile_name = info_file.readline()[:-1]
        info_file.close()
        user_agent = self.request.headers['User-Agent']
        is_pc = True
        if "Android" in user_agent or "iPad" in user_agent or \
                "iPhone" in user_agent or "iPod" in user_agent:
            img_name = img_mobile_name
            #replace for txt file
            img_style = "width:300px"
            time_points = []
            is_pc = False
        elif "Windows" in user_agent or "Linux" in user_agent:
            img_name = img_pc_name
            img_style = "width:720px;height:50px;"
        else:
            img_name = img_pc_name
            img_style = "width:720px;height:50px;"
        
            
        self.render('jumbotron.html', filename=img_name, is_pc=is_pc,
                    dt=dt, time_points=time_points, user_agent=user_agent)


    def get_time_points(self, start_dt_str):
        start_datetime = datetime.datetime.strptime(start_dt_str, 
                                                    "%H:%M %d.%m.%Y")
        delta_time = datetime.timedelta(minutes=0)
        time_points = []
        while(delta_time < datetime.timedelta(hours=2, minutes=1)):
            time_point = (start_datetime + delta_time).strftime("%H:%M")
            time_points.append(time_point)
            delta_time += datetime.timedelta(minutes=10)
        return time_points


class Application(tornado.web.Application):
    def __init__(self):
        handlers=[
            (r'/', MainHandler),
            (r"/img/(.*)", tornado.web.StaticFileHandler, {'path': "website/img"}),
            (r"/css/(.*)", tornado.web.StaticFileHandler, {'path': "website/css"}),
            (r"/js/(.*)", tornado.web.StaticFileHandler, {'path': "website/js"}),
            (r"/fonts/(.*)", tornado.web.StaticFileHandler, {'path': "website/fonts"}),
#             (r"/txt/(.*)", tornado.web.StaticFileHandler, {'path': "./txt"}),
        ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "resources"),
#             xsrf_cookies=True,
#             cookie_secret=b64encode(os.urandom(64)),
#             debug=True,
#             auto_reload=True,
        )
        tornado.web.Application.__init__(self, handlers, **settings)

# def make_app():
#     return tornado.web.Application([
#         (r"/", MainHandler),
#         (r"/img", ImgHandler),
#     ])


def loop():
    port = 80
    print("WEBSITE: Site is started on port", port)
    print("WEBSITE: Open localhost:%d in your browser to see forecast" % port)
    print("WEBSITE: In case of inconvenience, use translation from Russian language to your own")
    app = Application()
    app.listen(port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    loop()
