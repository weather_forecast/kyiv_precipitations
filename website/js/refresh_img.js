device = ""
var d = new Date();
var minutes = d.getMinutes();	

if (document.getElementById("pc")) {
	device = "pc";
}
else if (document.getElementById("mobile")) {
	device = "mobile";
}

// try to refresh every 5 seconds 
// but refresh only if a new minute is started
setInterval(function(){
	var date = new Date();
	// leave 5 seconds for picture update
	if (minutes != date.getMinutes() && date.getSeconds() >= 5)
	{
		var imgFilename = "/img/forecast_" + device + "_image";
		// time format is 2018-10-07_20-53-00
		imgFilename += "_" + date.getFullYear().toString();
		imgFilename += "-" + concatZero(date.getMonth() + 1); // month is in range 0-11
		imgFilename += "-" + concatZero(date.getDate());
		imgFilename += "_" + concatZero(date.getHours());
		imgFilename += "-" + concatZero(date.getMinutes());
		//imgFilename += "-00"; // 00 seconds
		
		imgFilename += ".png";
		
		elementToUpdate = "img#" + device;
		
	//	alert(elementToUpdate);
	//	alert(imgFilename);
        $.get(imgFilename)
            .done(function() 
            { 
                $(elementToUpdate).attr("src", imgFilename);
                minutes = date.getMinutes(); 
            }).fail(function() 
            { 
                /*
                imgFilename = imgFilename.slice(0, -5);
                imgFilename +='1.png';
                $(elementToUpdate).attr("src", imgFilename);
                minutes = date.getMinutes();
                */
            })
	}
}, 5000);

function concatZero(x) { 
	if (x >= 0 && x < 10)
	{
		return "0" + x.toString();
	}
	else
	{
		return x.toString();
	}
}