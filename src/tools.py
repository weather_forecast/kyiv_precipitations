import numpy as np
from PIL import Image, ImageDraw, ImageFilter, ImageFont
import datetime, urllib.request, os.path, math
import matplotlib
import matplotlib.pyplot as plt
import shutil
import ssl
import importlib

import src.config as cnf


class Rectangle:
    def __init__(self, top = 0, left = 0, bottom = 0, right = 0):
        self.top = top
        self.left = left
        self.bottom = bottom
        self.right = right


def extract_weather_map(image, options=None, **kwargs):
    """
    Subtract clear map image from actual weather snapshot
    """
#     import os
    if options is None:
        options = []
    if 'filename' in kwargs.keys():
        if os.path.isfile("output/weather_maps/weather_map " + kwargs['filename'] + ".jpg"):
            image = Image.open("output/weather_maps/weather_map " + kwargs['filename'] + ".jpg")
            if "print" in options:
                print("TOOLS: Weather map", kwargs['filename'], "was previously extracted")
            return image
    image_free = Image.open(cnf.CLEAR_MAP_PATH)
#     snapshot_path = "snapshots/Ukr_J 2016-09-29 16-39-00.jpg"
    image = image.copy()
    draw = ImageDraw.Draw(image) #
    width = image.size[0] 
    height = image.size[1]     
    pix_free = image_free.load()
    pix_snapshot = image.load()
    # pix_clear = deepcopy(pix_snapshot)
#     cloud_area = 0
#     cloud_map = np.ones((height, width))
    for i in range(width):
        for j in range(height):
            a = 255 - abs(pix_snapshot[i, j][0] - pix_free[i, j][0])
            b = 255 - abs(pix_snapshot[i, j][1] - pix_free[i, j][1])
            c = 255 - abs(pix_snapshot[i, j][2] - pix_free[i, j][2])
            
            draw.point((i, j), (a, b, c))
            
    if "save" in options:
        try:
            image.save("output/weather_maps/weather_map " + kwargs['filename'] + ".jpg", "JPEG")
        except:
            image.save("output/weather_map.jpg", "JPEG")
        
    return image

def convert_color_to_binary_map_and_filter(image, options=None, **kwargs):
    """
    Reduce color weather map to black-white cloud map
    """
    if options is None:
        options = []
    image = image.copy()
    width = image.size[0]
    height = image.size[1]
    draw = ImageDraw.Draw(image)
    pixels = image.load()
    for x in range(width):
        for y in range(height):
            a, b, c = pixels[x, y]
            if (a, b, c) != (255, 255, 255):
                draw.point((x, y), (0, 0, 0))
#             else:
#                 draw.point((i, j), (0, 0, 0))
    if "save" in options:
        try:
            image.save("output/cloud_map " + kwargs['filename'] + ".jpg", "JPEG")
        except:
            image.save("output/cloud_map.jpg", "JPEG")
    image_median_filtered = image.filter(ImageFilter.MedianFilter(size=7))
    pixels = image_median_filtered.load()
    cloud_map = np.zeros((height, width))
    for i in range(width):
        for j in range(height):
            cloud_map[j][i] = 1 if pixels[i, j] != (255, 255, 255) else 0
    
    if "save" in options:
        try:
            image_median_filtered.save("output/filt_cloud_map " + kwargs['filename'] + ".jpg", "JPEG")
        except:
            image_median_filtered.save("output/filt_cloud_map.jpg", "JPEG")
    
    return cloud_map, image

def find_cloud_edges(image, options):
    image = image.copy()
    image_edges = image.filter(ImageFilter.FIND_EDGES)
    return image_edges
#     image_edges.show()


def shift_binary_map(binary_map, v_shift, h_shift):
    shifted_map = np.zeros(binary_map.shape)
    for i in range(binary_map.shape[0]):
        for j in range(binary_map.shape[1]):
            if binary_map[i][j] == 1 and i + v_shift < binary_map.shape[0] and \
                    j + h_shift < binary_map.shape[1] and i + v_shift > 0 and \
                    j + h_shift > 0:
                shifted_map[i + v_shift][j + h_shift] = 1
                
    return shifted_map


def calc_binary_correlation(map_1, map_2, correlation_region = None):
    """
    Sum coincide units in 0-1 maps in correlation_region, 
    that defines rectangle for summation. 
    Need to maximize value of a function
    """
    if map_1.shape != map_2.shape:
        print("WIND: Maps in calc_binary_correlation() have different sizes")
        return
    if correlation_region is None:
        correlation_region = Rectangle(0, 0,  map_1.shape[0], map_1.shape[1])
    corr = 0
    for i in range(correlation_region.top, correlation_region.bottom):
        for j in range(correlation_region.left, correlation_region.right):
            if map_1[i][j] == map_2[i][j] and map_1[i][j] == 1:
                corr += 1
    
    return corr


def find_best_shift_between_binary_maps(binary_map_prev, binary_map_actual):
#     h, w = binary_map_prev.shape
    if binary_map_actual.shape != binary_map_prev.shape:
        print("ERROR: Cloud maps in find_best_shift_between_binary_maps() have different sizes")
        return
    
    rect_prev = calc_cloud_rectangle(binary_map_prev)
    rect_actual = calc_cloud_rectangle(binary_map_actual)
    v_shift_begin = rect_actual.bottom - (rect_prev.bottom - rect_prev.top)
    v_shift_end = rect_actual.top - rect_prev.top
    h_shift_begin = rect_actual.left - rect_prev.left
    h_shift_end = rect_actual.right - (rect_prev.right - rect_prev.left)
#     vert_range_step = 1 if vertical_shift > 0 else -1
#     horizont_range_step = 1 if horizontal_shift > 0 else -1
    max_corr = -1
    max_corr_i = 0
    max_corr_j = 0
    for i in range(min(v_shift_begin, v_shift_end), max(v_shift_begin, v_shift_end)):
        for j in range(min(h_shift_begin, h_shift_end), max(h_shift_begin, h_shift_end)):
            shifted_cloud_map_prev = shift_binary_map(binary_map_prev, i, j)
            correlation_region = Rectangle()
            correlation_region.top = min(rect_prev.top + i, rect_actual.top)
            correlation_region.left = min(rect_prev.left + j, rect_actual.left)
            correlation_region.bottom = max(rect_prev.bottom + i, rect_actual.bottom)
            correlation_region.right = max(rect_prev.right + i, rect_actual.right)
            corr = calc_binary_correlation(binary_map_actual, shifted_cloud_map_prev, 
                                       correlation_region)
            if corr > max_corr:
                max_corr = corr
                max_corr_i = i
                max_corr_j = j
    
    return max_corr_i, max_corr_j


def shift_color_map(image, v_shift, h_shift):
    image = image.copy()
    width = image.size[0]
    height = image.size[1]
    draw = ImageDraw.Draw(image)
    pixels = image.load()
    color_map = 255*np.ones((height, width, 3), dtype=int)
    for x in range(width):
        for y in range(height):
            if pixels[x, y] != (255, 255, 255) and width > x + h_shift > 0 and height > y + v_shift > 0:
                for k in range(3):
                    color_map[y + v_shift, x + h_shift, k] = pixels[x, y][k]
                    
    for x in range(width):
        for y in range(height):
            color_tuple = tuple(color_map[y, x][0:3])
            draw.point([x, y], color_tuple)
    
#     image.show() 
    return image


def calc_color_correlation(w_map_1, w_map_2, 
                           correlation_region = None):
    pxl_1 = w_map_1.load()
    pxl_2 = w_map_2.load()
    if w_map_1.size != w_map_2.size:
        print("ERROR: Weather maps in calc_color_correlation() have different sizes")
        return
    width = w_map_1.size[0]
    height = w_map_1.size[1]
    
    if correlation_region is None:
        correlation_region = Rectangle(0, 0, height, width)
    else:
        correlation_region.left = max(0, correlation_region.left)
        correlation_region.right = min(width, correlation_region.right)
        correlation_region.top = max(0, correlation_region.top)
        correlation_region.bottom = min(height, correlation_region.bottom)
    
    corr = 0

    for x in range(correlation_region.left, correlation_region.right):
        for y in range(correlation_region.top, correlation_region.bottom):
            for k in range(3):
                corr += abs(pxl_1[x, y][k] - pxl_2[x, y][k])
    
    return corr
    
    
def find_best_shift_between_color_maps(color_map_prev, color_map_actual,
                                       options=None):
    """
    Returns tuple with optimal vertical and horizontal shift to maximize 
    correlation between <color_map_prev> and <color_map_actual>
    """
#     h, w = color_map_prev.shape
    if options is None:
        options = []
    if color_map_actual.size != color_map_prev.size:
        print("ERROR: Cloud maps in find_best_shift_of_binary_maps() have different sizes")
        return
    binary_map_prev, _ = convert_color_to_binary_map_and_filter(color_map_prev)
    binary_map_actual, _ = convert_color_to_binary_map_and_filter(color_map_actual)
    rect_prev = calc_cloud_rectangle(binary_map_prev)
    rect_actual = calc_cloud_rectangle(binary_map_actual)
    v_shift_begin = rect_actual.bottom - (rect_prev.bottom - rect_prev.top)
    v_shift_end = rect_actual.top - rect_prev.top
    h_shift_begin = rect_actual.left - rect_prev.left
    h_shift_end = rect_actual.right - (rect_prev.right - rect_prev.left)
#     vert_range_step = 1 if vertical_shift > 0 else -1
#     horizont_range_step = 1 if horizontal_shift > 0 else -1
    import sys
    min_corr = sys.maxsize
    max_corr_i = 0
    max_corr_j = 0
    best_shifted_weather_map_prev = color_map_prev.copy()
    for i in range(min(v_shift_begin, v_shift_end), max(v_shift_begin, v_shift_end)):
        for j in range(min(h_shift_begin, h_shift_end), max(h_shift_begin, h_shift_end)):
            shifted_weather_map_prev = shift_color_map(color_map_prev, i, j)
            correlation_region = Rectangle()
            correlation_region.top = min(rect_prev.top + i, rect_actual.top)
            correlation_region.left = min(rect_prev.left + j, rect_actual.left)
            correlation_region.bottom = max(rect_prev.bottom + i, rect_actual.bottom)
            correlation_region.right = max(rect_prev.right + i, rect_actual.right)
            corr = calc_color_correlation(color_map_actual, shifted_weather_map_prev, 
                                       correlation_region)
            if corr < min_corr:
                min_corr = corr
                max_corr_i = i
                max_corr_j = j
                best_shifted_weather_map_prev = shifted_weather_map_prev.copy()
    if "save" in options:
        best_shifted_weather_map_prev.save("output/_best_shifted_map.jpg", "JPEG")
        color_map_actual.save("output/_actual_map.jpg", "JPEG")
    return max_corr_i, max_corr_j


def calc_cloud_rectangle(test):
    """
    Calculates number of clouds in cloud map <test>
    Finds top left and right bottom angle of clouds
    """
    
    in_angles = np.array([
        [[1,1],
         [1,0]],
        [[1,1],
         [0,1]],
        [[1,0],
         [1,1]],
        [[0,1],
         [1,1]],
        ])
    out_angles = np.array([
        [[0,0],
         [0,1]],
        [[0,0],
         [1,0]],
        [[0,1],
         [0,0]],
        [[1,0],
         [0,0]],
        ])
    
    h, w = test.shape
    out_number = 0
    in_number = 0
    cloud_rectangle = Rectangle(h, w, 0, 0)
#     top_left_corner = [h, w]
#     bottom_right_corner = [0, 0]
    for i in range(h - 1):
        for j in range(w - 1):
            square = np.zeros((2,2))
            square[0, 0] = test[i, j]
            square[1, 0] = test[i + 1, j]
            square[0, 1] = test[i, j + 1]
            square[1, 1] = test[i + 1, j + 1]
            for in_angle in in_angles:
                if np.array_equal(square, in_angle):
                    in_number += 1
            for i_out_angle, out_angle in enumerate(out_angles):
                if np.array_equal(square, out_angle):
                    out_number += 1
                    if i_out_angle == 0: # top left corner of cloud
                        if i < cloud_rectangle.top:
                            cloud_rectangle.top = i
                        if j < cloud_rectangle.left:
                            cloud_rectangle.left = j
                    if i_out_angle == 3: # right bottom corner of cloud
                        if i > cloud_rectangle.bottom:
                            cloud_rectangle.bottom = i
                        if j > cloud_rectangle.right:
                            cloud_rectangle.right = j
    cloud_number = (in_number - out_number) / 4
    if cloud_number == 0:
        print("TOOLS: There are no clouds in the sky")
    elif abs(cloud_number) == 1:
        print("TOOLS: There is 1 cloud in the sky")
    else:
        print("TOOLS: There are", abs(cloud_number), "clouds in the sky")
#     print("Top left corner:", top_left_corner)
#     print("Right bottom corner:", bottom_right_corner)
    
    return cloud_rectangle

'''
def convert_color_to_binary_map_and_filter(image, options = [], **kwargs):
    """
    Reduce color weather map to black-white cloud map
    """
    from PIL import ImageDraw, ImageFilter
    image = image.copy()
    width = image.size[0]
    height = image.size[1]
    draw = ImageDraw.Draw(image)
    pixels = image.load()
    for i in range(width):
        for j in range(height):
            a, b, c = pixels[i, j]
            if (a, b, c) != (255, 255, 255):
                draw.point((i, j), (0, 0, 0))
#             else:
#                 draw.point((i, j), (0, 0, 0))
    if "save" in options:
        try:
            image.save("output/cloud_map " + kwargs['filename'] + ".jpg", "JPEG")
        except:
            image.save("output/cloud_map.jpg", "JPEG")
    image_median_filtered = image.filter(ImageFilter.MedianFilter(size=7))
    pixels = image_median_filtered.load()
    cloud_map = np.zeros((height, width))
    for i in range(width):
        for j in range(height):
            cloud_map[j][i] = 1 if pixels[i, j] != (255, 255, 255) else 0
    
    if "save" in options:
        try:
            image_median_filtered.save("output/filt_cloud_map " + kwargs['filename'] + ".jpg", "JPEG")
        except:
            image_median_filtered.save("output/filt_cloud_map.jpg", "JPEG")
    
    return cloud_map, image
'''

def convert_dt_str_to_snapshot(dt):
#     import datetime
#     datetime.datetime.strptime(dt, "%Y-%m-%d %H-%M-%S")
    
    return Image.open("" + cnf.SNAPSHOT_FOLDER + "/Ukr_J " + dt + ".jpg")


def grab_snapshot(snapshot_dt_str):
    url_path = "https://meteo.gov.ua/radars/Ukr_J%20" + snapshot_dt_str + ".jpg"
    local_path = "snapshots/Ukr_J " + snapshot_dt_str + ".jpg"
    
    tls_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
    if not os.path.isfile(local_path):
        try:
            url_path = url_path.replace(" ", "%20")
            with urllib.request.urlopen(url_path, context=tls_context) as response, open(local_path, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)
            #urllib.request.urlretrieve(url_path, local_path)
        except BaseException as e:
            print("TOOLS: Error in grab_snapshot():", e)
            return -1 # Screenshot is absent
        if os.path.isfile(local_path):
            if os.path.getsize(local_path) < 190 * 1024:
                os.remove(local_path)
                return 0 # Screenshot is crashed
            else:
                return 1 # Screenshot is downloaded
        else:
            return -1
    else:
        return 2 # Screenshot is previously downloaded


def get_actual_datetime():
    now_dt = datetime.datetime.now()
    now_str = now_dt.strftime("%Y-%m-%d %H-%M")
    now_dt_round = datetime.datetime.strptime(now_str, "%Y-%m-%d %H-%M")
    now_minutes = int(now_str[-2:])
    actual_snapshot_dt = now_dt_round - datetime.timedelta(minutes=11 + (now_minutes % 10))
    return actual_snapshot_dt
    
def get_actual_datetime_string():
    now_dt = datetime.datetime.now()
    now_str = now_dt.strftime("%Y-%m-%d %H-%M")
    now_dt_round = datetime.datetime.strptime(now_str, "%Y-%m-%d %H-%M")
    now_minutes = int(now_str[-2:])
    actual_snapshot_dt = now_dt_round - datetime.timedelta(minutes=11 + (now_minutes % 10))
    actual_snapshot_dt_str = actual_snapshot_dt.strftime("%Y-%m-%d %H-%M-%S")
    return actual_snapshot_dt_str

def get_last_available_snapshot_datetime_for_datetime(now_dt):
    now_str = now_dt.strftime("%Y-%m-%d %H-%M")
    now_dt_round = datetime.datetime.strptime(now_str, "%Y-%m-%d %H-%M")
    now_minutes = int(now_str[-2:])
    actual_snapshot_dt = now_dt_round - datetime.timedelta(minutes=11 + (now_minutes % 10))
    return actual_snapshot_dt


def get_delta_time_for_timezone():
    # from April till October !!!!!!! REVISE !!!!!!!
#     if 11 > int(now_dt.strftime("%m")) > 3:
#         return datetime.timedelta(hours=3)
#     else:
#         return datetime.timedelta(hours=2)
    return datetime.datetime.now() - datetime.datetime.utcnow()

def save_result_of_cloud_shift(last_calculation_datetime, x_shift, y_shift, time_shift = 0.5,
                               options=None):
    if options is None:
        options = []
    save_datetime = (get_last_available_snapshot_datetime_for_datetime(last_calculation_datetime) +
                     get_delta_time_for_timezone())
    file_name_datetime = save_datetime.strftime("%Y-%m-%d %H-%M-%S")
    date_folder = save_datetime.strftime("%Y-%m-%d")
    save_folder = "output/wind_shifts/" + date_folder + "/"
    if not os.path.isdir(save_folder):
        os.mkdir(save_folder)
    files_to_save = [open("output/_wind_shift.txt", "w"), 
                     open(save_folder + "wind_shift " + file_name_datetime + ".txt", "w")]
    file_text_datetime = save_datetime.strftime("%H:%M %d.%m.%Y")

    x_shift = int(x_shift * 0.5 / time_shift)
    y_shift = int(y_shift * 0.5 / time_shift)

    for file_to_save in files_to_save:
        file_to_save.write(file_text_datetime + "\n")
        file_to_save.write(str(x_shift) + "\n")
        file_to_save.write(str(y_shift) + "\n")
        file_to_save.close()
    if "print" in options:

        if x_shift is not None and y_shift is not None:
            winter_speed = (x_shift**2 + y_shift**2)**0.5
            winter_azimuth = 180*math.atan2(x_shift, -y_shift)/math.pi
            winter_azimuth += 360 if winter_azimuth < 0 else 0
            print("WIND: datetime of last available weather map:", file_text_datetime)
            print("WIND: calculated properties: Speed: %.0f km/h, Azimuth: %.0f deg" % (winter_speed, winter_azimuth))
            
        else:
            print("WIND: properties are not available (there are no clouds in the sky)")
    
def get_result_of_cloud_shift():
    file_to_take = open("output/_wind_shift.txt", "r")
    last_cloud_shift_calculation = file_to_take.readline()[:-1]
    x_shift = file_to_take.readline()[:-1]
    y_shift = file_to_take.readline()[:-1]
    if x_shift != 'None' and y_shift != 'None':
        x_shift = int(x_shift)
        y_shift = int(y_shift)
    else:
        x_shift, y_shift = None, None
    last_cloud_shift_calculation_datetime = \
            datetime.datetime.strptime(last_cloud_shift_calculation, "%H:%M %d.%m.%Y")
    file_to_take.close()
    
    return last_cloud_shift_calculation_datetime, x_shift, y_shift

def get_weather_map_for_datetime(now_dt = None, options=None, **kwargs):
#     import datetime
#     if "test_mode" in options:
#         if 'test_datetime_str' in kwargs.keys():
#             test_datetime_str = kwargs['test_datetime_str']
#             now_dt = datetime.datetime.strptime(test_datetime_str, "%Y-%m-%d %H-%M-%S")
    if options is None:
        options = []
    if now_dt is None:
            now_dt = datetime.datetime.utcnow()
    now_str = now_dt.strftime("%Y-%m-%d %H-%M")
    now_dt_round = datetime.datetime.strptime(now_str, "%Y-%m-%d %H-%M")
    now_minutes = int(now_str[-2:])
    actual_snapshot_dt = now_dt_round - datetime.timedelta(minutes=11 + (now_minutes % 10))
    actual_snapshot_dt_str = actual_snapshot_dt.strftime("%Y-%m-%d %H-%M-%S")
    if (grab_snapshot(actual_snapshot_dt_str) > 0):
        snapshot = convert_dt_str_to_snapshot(actual_snapshot_dt_str)
        return extract_weather_map(snapshot, options, 
                                   filename=actual_snapshot_dt_str)
    else:
        return None


def check_point_in_image_borders(image, x, y):
    return (0 < x < image.size[0] and 0 < y < image.size[1])


start_minute_10_remainder = -1
def is_it_time_to_start_calculation(start_datetime):
    global start_minute_10_remainder
    run_every_minutes = 10
    start_minute = int(start_datetime.strftime("%M"))
    if start_minute_10_remainder == -1:
        start_minute_10_remainder = start_minute % run_every_minutes
    if not start_minute % run_every_minutes == start_minute_10_remainder:
        print("WIND: Calculation of wind properties is waiting for the new snapshot")
        return False
    else:
        return True


def get_now_datetime_rounded(now_dt = None):
    if now_dt is None:
        now_dt = datetime.datetime.utcnow() 
    now_str = now_dt.strftime("%Y-%m-%d %H-%M")
    now_dt_round = datetime.datetime.strptime(now_str, "%Y-%m-%d %H-%M")
    return now_dt_round


def calc_minutes_between_now_and_last_snapshot(now_dt = None):
    if now_dt is None:
        now_dt = datetime.datetime.utcnow()
    now_str = now_dt.strftime("%Y-%m-%d %H-%M")
    now_dt_round = get_now_datetime_rounded()
    now_minutes = int(now_str[-2:])
    actual_snapshot_dt = now_dt_round - datetime.timedelta(minutes=11 + (now_minutes % 10))
    minutes_since_last_snapshot = int((now_dt_round - actual_snapshot_dt).seconds/cnf.SECONDS_IN_MINUTE)
    return minutes_since_last_snapshot
    
    
def make_forecast_for_point(weather_image, max_x_shift, max_y_shift, x_point, y_point):
    weather_map = weather_image.load()
    
    wind_speed_pixels_in_hours = (1 / cnf.WIND_TIME_SHIFT_IN_HOURS) * \
                                 (max_x_shift**2 + max_y_shift**2)**0.5
    # reverse wind direction
    max_x_shift = -max_x_shift
    max_y_shift = -max_y_shift
    time_color_forecast = np.zeros((cnf.MINUTES_IN_HOUR * cnf.FORECAST_HOURS, 3), dtype=int)
    minutes_since_last_snapshot = calc_minutes_between_now_and_last_snapshot()
    init_x_shift = int(max_x_shift * minutes_since_last_snapshot / float(cnf.MINUTES_IN_HOUR))
    init_y_shift = int(max_y_shift * minutes_since_last_snapshot / float(cnf.MINUTES_IN_HOUR))

    for time_in_minutes, shift_in_wind_direction in enumerate(np.linspace(0,
                                                                          wind_speed_pixels_in_hours * cnf.FORECAST_HOURS,
                                                                          cnf.MINUTES_IN_HOUR * cnf.FORECAST_HOURS)):
        try:
            x_shift = int(x_point + init_x_shift +
                          shift_in_wind_direction * max_x_shift / wind_speed_pixels_in_hours)
            y_shift = int(y_point + init_y_shift +
                          shift_in_wind_direction * max_y_shift / wind_speed_pixels_in_hours)

            if check_point_in_image_borders(weather_image, x_shift, y_shift):
                color_tuple_at_time_point = weather_map[x_shift, y_shift]
                time_color_forecast[time_in_minutes][0:3] = color_tuple_at_time_point
            else:
                time_color_forecast[time_in_minutes][0:3] = cnf.WHITE_COLOR_TUPLE
            '''!!! Need to replace warnings to other analysis script !!!'''
#             if not np.array_equal(color_tuple_at_time_point, cnf.WHITE_COLOR_TUPLE):
#                 if not is_weather_warning_printed:
#                     print("\n!!! WEATHER FORSENING at %d minutes" % time_in_minutes)
#                     is_weather_warning_printed = True


#             print("x_shift =", x_shift,)
#             print("y_shift =", y_shift,)
#             print("wind_speed_pixels_in_hours =", wind_speed_pixels_in_hours,)
#             print("max_x_shift =", max_x_shift,)
#             print("max_y_shift =", max_y_shift,)
#             print("shift_in_wind_direction =", shift_in_wind_direction)
        except BaseException as e:
            print("FORECAST: exception at make_forecast_for_point():", e)
            time_color_forecast[time_in_minutes][0:3] = cnf.WHITE_COLOR_TUPLE[0:3]

    return time_color_forecast


def draw_pc_oriented_line_image(time_forecast):# drawing PC oriented line image
    forecast_image = Image.new('RGBA', 
                               (cnf.WEATHER_FORECAST_IMAGE_WIDTH + 2*cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING, 
                                cnf.WEATHER_FORECAST_IMAGE_HEIGHT + cnf.WEATHER_FORECAST_IMAGE_TEXT_HEIGHT_PADDING), 
                                cnf.WHITE_COLOR_TUPLE)
    
    draw = ImageDraw.Draw(forecast_image)
    proportion = (float(cnf.MINUTES_IN_HOUR) * cnf.FORECAST_HOURS/cnf.WEATHER_FORECAST_IMAGE_WIDTH)
    
    for x in range(cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING, 
                   cnf.WEATHER_FORECAST_IMAGE_WIDTH + cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING):
        i = int((x - cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING) * proportion)
        color_list = list(time_forecast[i])
        color_list.append(255)
        color_tuple = tuple(color_list)
        for y in range(cnf.WEATHER_FORECAST_IMAGE_HEIGHT):
            draw.point((x,y), color_tuple)
    
    return forecast_image


# def get_circle_box_for_distance_from_border(distance_from_border):
#     return (distance_from_border, distance_from_border, 
#             cnf.WEATHER_FORECAST_MOBILE_IMAGE_WIDTH- distance_from_border, 
#             cnf.WEATHER_FORECAST_MOBILE_IMAGE_HEIGHT - distance_from_border)

def draw_mobile_oriented_circle_image(time_forecast):
    sizes = [1] * len(time_forecast)
    #colors = ['yellowgreen', 'gold', 'lightskyblue', 'lightcoral']
    colors = [tuple(color_at_minute / 255.0) for color_at_minute in time_forecast] 
#     explode = (0.1, 0.1, 0.1, 0.1)  # explode a slice if required
    fig, ax = plt.subplots(1, 1, figsize=(9, 8))
    #fig = plt.gcf()
    ax.pie(sizes, explode=None, wedgeprops={'linewidth':0}, 
            colors=colors, shadow=False, startangle=90, counterclock=False)
            
    #draw a circle at the center of pie to make it look like a donut
    centre_circle = plt.Circle((0,0),0.85,color=(238.0/255, 238.0/255, 238.0/255), 
                               fc=(238.0/255, 238.0/255, 238.0/255),linewidth=0.0)
    #fig = plt.gcf()
    #fig.gca().add_artist(centre_circle)
    ax.add_artist(centre_circle)
    
    ax.plot([0, 0], [0.855, 1], "k-", linewidth=3)
    
    # Set aspect ratio to be equal so that pie is drawn as a circle.
    ax.axis('equal')
    
    #fig = plt.gcf()
    #ax = fig.gca()
    
#     plt.show()  
    return fig, ax


def write_time_points_at_pc_figure(forecast_pc_image, start_datetime):
    draw = ImageDraw.Draw(forecast_pc_image)
#     minute = start_datetime.minute
#     start_datetime_str = start_datetime.strftime("%H:%M")
    ''' prepare text to be written on image '''
#     start_datetime_on_image = get_last_available_snapshot_datetime_for_datetime(start_datetime) + \
#             get_delta_time_for_timezone(start_datetime)
    """ make forecast from actual time """
    start_datetime_on_image = start_datetime + get_delta_time_for_timezone()
    time_point = start_datetime_on_image
    time_points_list = []
    while (time_point - start_datetime_on_image <= datetime.timedelta(hours=cnf.FORECAST_HOURS)):
        datetime_str = time_point.strftime("%H:%M")
        time_points_list.append(datetime_str)
        time_point += datetime.timedelta(minutes=10)
#     draw.text()
    # lines on image
    x_coords_time_points = range(cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING, 
          cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING + cnf.WEATHER_FORECAST_IMAGE_WIDTH + cnf.WIDTH_STEP_BETWEEN_TEXT_ON_PC_IMAGE, 
          cnf.WIDTH_STEP_BETWEEN_TEXT_ON_PC_IMAGE)
    for i, x in enumerate(x_coords_time_points):
#         y_points_of_dots = list(range(5))
#         y_points_of_dots.extend(
#         range(cnf.WEATHER_FORECAST_IMAGE_HEIGHT-6, cnf.WEATHER_FORECAST_IMAGE_HEIGHT))
        # do not make dashes on first and last points
        if i != 0 and i != len(x_coords_time_points) - 1:
            y_points_of_dots = list(range(cnf.WEATHER_FORECAST_IMAGE_HEIGHT-6, cnf.WEATHER_FORECAST_IMAGE_HEIGHT))
            for y in y_points_of_dots:
                draw.point((x,y), cnf.BLACK_COLOR_TUPLE)
        x_position_of_text = x - cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING/2 - 2
        y_position_of_text = cnf.WEATHER_FORECAST_IMAGE_HEIGHT+cnf.WEATHER_FORECAST_IMAGE_TEXT_HEIGHT_PADDING/2
        draw.text((x_position_of_text,y_position_of_text), 
                  time_points_list[i],
                  fill=(0,0,0,255),
                  font=ImageFont.truetype("arial.ttf", 14))
    """
    # attempt to make better quality text on forecast image
    q = 1 # number of times to increase picture to make higher quality text
    whole_pc_image_size = (cnf.WEATHER_FORECAST_IMAGE_WIDTH + 2*cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING, 
                           cnf.WEATHER_FORECAST_IMAGE_HEIGHT + cnf.WEATHER_FORECAST_IMAGE_TEXT_HEIGHT_PADDING)
    bigger_image_size = (q*(cnf.WEATHER_FORECAST_IMAGE_WIDTH + 2*cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING), 
                         q*(cnf.WEATHER_FORECAST_IMAGE_HEIGHT + cnf.WEATHER_FORECAST_IMAGE_TEXT_HEIGHT_PADDING))
    
    bigger_image_to_write_text_on = forecast_pc_image.resize(bigger_image_size, Image.LANCZOS)
#     bigger_image_to_write_text_on = forecast_pc_image
    draw_big_image = ImageDraw.Draw(bigger_image_to_write_text_on)
    
    for i, x in enumerate(x_coords_time_points): 
        x_position_of_text = x - cnf.WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING/2 - 2
        y_position_of_text = cnf.WEATHER_FORECAST_IMAGE_HEIGHT+cnf.WEATHER_FORECAST_IMAGE_TEXT_HEIGHT_PADDING/2 - 5
        draw_big_image.text((q*x_position_of_text, q*y_position_of_text), 
                             time_points_list[i],
                             fill=(0,0,0,255),
                             font=ImageFont.truetype("arial.ttf", 14*q))
    # anti-aliasing
    forecast_pc_image = bigger_image_to_write_text_on.resize(whole_pc_image_size, Image.LANCZOS)
    
    return forecast_pc_image
    """
            


def write_time_points_at_mobile_figure(ax, start_datetime):
    fontsize = 20
    # last update time
#     print_datetime = get_last_available_snapshot_datetime_for_datetime(start_datetime) + \
#             get_delta_time_for_timezone(start_datetime)
    """ make forecast from actual time """        
    print_datetime = start_datetime + get_delta_time_for_timezone()
    print_datetime_str = print_datetime.strftime("%H:%M")
    ax.text(-0.12, 0.7, print_datetime_str, fontsize=fontsize)
    # +30 minutes
    print_datetime = print_datetime + datetime.timedelta(minutes=30)
    print_datetime_str = print_datetime.strftime("%H:%M")
    ax.text(1.05, 0.0, print_datetime_str, fontsize=fontsize)
    # +1 hour
    print_datetime = print_datetime + datetime.timedelta(minutes=30)
    print_datetime_str = print_datetime.strftime("%H:%M")
    ax.text(-0.12, -1.15, print_datetime_str, fontsize=fontsize)
    # +1.5 hours
    print_datetime = print_datetime + datetime.timedelta(minutes=30)
    print_datetime_str = print_datetime.strftime("%H:%M")
    ax.text(-1.3, 0.0, print_datetime_str, fontsize=fontsize)
    # + 2 hours
    print_datetime = print_datetime + datetime.timedelta(minutes=30)
    print_datetime_str = print_datetime.strftime("%H:%M")
    ax.text(-0.12, 1.1, print_datetime_str, fontsize=fontsize)


def generate_image_paths_and_save_txt(start_datetime, options):
    # calc datetime for image filenames and print info
#     save_datetime = get_last_available_snapshot_datetime_for_datetime(start_datetime) + \
#             get_delta_time_for_timezone(start_datetime)
#     
#     date_folder = save_datetime.strftime("%Y-%m-%d")
#     print_datetime = save_datetime
#     print_datetime_str = print_datetime.strftime("%H:%M %d.%m.%Y")
    save_datetime = datetime.datetime.now()
    date_folder = save_datetime.strftime("%Y-%m-%d")
    save_datetime_str = save_datetime.strftime("%Y-%m-%d %H-%M")
    print_datetime_str = save_datetime.strftime("%H:%M %d.%m.%Y")
    # for sites
    file_to_save = open("website/resources/last_update_datetime.txt", "w")
    file_to_save.write(print_datetime_str + "\n")
    
    filename_prefixes = ["pc", "mobile"]
    extensions = {"pc": ".png", "mobile" :".png"}
    paths = {}
    for name_prefix in filename_prefixes:
        if "real" in options:
            filename_prefix = "real_weather_" + name_prefix
        else:
            filename_prefix = "forecast_" + name_prefix
        save_output_path = "output/_" + filename_prefix +"_image" + extensions[name_prefix]
#         forecast_images[name_prefix].save("output/_" + filename_prefix +"_image.jpg")
        # folders for saving
        general_save_folder = "output/" + filename_prefix + "_images/"
        if not os.path.isdir(general_save_folder):
            os.mkdir(general_save_folder)
        folder_save = general_save_folder + date_folder + "/"
        if not os.path.isdir(folder_save):
            os.mkdir(folder_save)
        
        save_dt_folder_path = folder_save + filename_prefix + "_image_" + \
                save_datetime_str + extensions[name_prefix]
#         forecast_images[name_prefix].save()
        if "print" in options:
            print("FORECAST: " + filename_prefix + " image generated at %s" % print_datetime_str)
        # for site
        site_picture_path = None
        if not "test" in options:
            save_picture_name = filename_prefix +"_image_" + \
                    save_datetime_str.replace(" ", "_") + extensions[name_prefix] 
            site_picture_path = "website/img/" + save_picture_name
#             forecast_images[name_prefix].save(site_picture_path)
            
            file_to_save.write(save_picture_name + "\n")
            
        paths[name_prefix] = [save_output_path, save_dt_folder_path, site_picture_path]
    file_to_save.close()
    
    return paths
        

def save_forecast_pc_image(image, path_list):
    for path in path_list:
        if path is not None:
            image.save(path, "PNG")


def save_forecast_mobile_figure(figure, path_list):
    for path in path_list:
        if path is not None:
            figure.savefig(path, dpi=90, format="png", transparent=True)


def convert_time_forecast_to_image(time_forecast, start_datetime, options=None):
    if options is None:
        options = []
    forecast_pc_image = draw_pc_oriented_line_image(time_forecast)
    write_time_points_at_pc_figure(forecast_pc_image, start_datetime)
    forecast_mobile_figure, subfigure = draw_mobile_oriented_circle_image(time_forecast)
    write_time_points_at_mobile_figure(subfigure, start_datetime)
    # saving
#     forecast_images = {"pc": forecast_pc_image, "mobile": forecast_mobile_image}
    image_paths = generate_image_paths_and_save_txt(start_datetime, options)
    save_forecast_pc_image(forecast_pc_image, image_paths["pc"])
    save_forecast_mobile_figure(forecast_mobile_figure, image_paths["mobile"])
    plt.clf()


def check_wind_calculation_relevance(start_datetime, last_wind_calculation_datetime,
                                     x_shift, y_shift):
    # TODO: add checking of difference between @start_datetime and @last_wind_calculation_datetime
    if (x_shift is None and y_shift is None) or (x_shift == 0 and y_shift == 0):
        return False
    return True

def check_is_weather_image_clear(weather_image):
    weather_map = weather_image.load()
    for x in range(weather_image.size[0]):
        for y in range(weather_image.size[1]):
            if weather_map[x, y] != cnf.WHITE_COLOR_TUPLE[0:3]:
                return False
    return True

def get_clear_forecast():
    return 255 * np.ones((cnf.MINUTES_IN_HOUR * cnf.FORECAST_HOURS, 3), dtype=int)