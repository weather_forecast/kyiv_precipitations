from ctypes import *
import numpy.ctypeslib as npct

WIDTH =         767
HEIGHT =        630
COLOR_NUMBER =  3

compiled_lib = npct.load_library("src/compiled_tools/tools", "")
first_level_array_type = COLOR_NUMBER * c_uint8
second_level_array_type = HEIGHT * first_level_array_type
third_level_array_type = WIDTH * second_level_array_type

def convert_pxl_to_c_color_map(pxl):
    color_map = third_level_array_type()
    for x in range(WIDTH):
        for y in range(HEIGHT):
            for z in range(COLOR_NUMBER):
                color_map[x][y][z] = pxl[x, y][z]
    return color_map

def compiled_calc_color_correlation(w_map_1, w_map_2):
    pxl_1 = w_map_1.load()
    pxl_2 = w_map_2.load()
    if w_map_1.size != w_map_2.size:
        print("WIND: Error! Weather maps in calc_color_correlation() have different sizes")
        return
#     width = w_map_1.size[0]
#     height = w_map_1.size[1]
    
    compiled_lib.calc_color_correlation.argtypes = [third_level_array_type, third_level_array_type]
    compiled_lib.calc_color_correlation.restype = c_int
    pxl_1_c = convert_pxl_to_c_color_map(pxl_1)
    pxl_2_c = convert_pxl_to_c_color_map(pxl_2)
                
    corr = compiled_lib.calc_color_correlation(pxl_1_c, pxl_2_c)
    return corr


def compiled_find_best_shift_between_color_maps(color_map_prev, color_map_actual,
                                                options=None):
    """
    Returns tuple with optimal vertical and horizontal shift to maximize 
    correlation between <color_map_prev> and <color_map_actual>
    """
    if options is None:
        options = []
    if "print" in options:
        import datetime
        print("WIND: Calculation of wind parameters was begun at", datetime.datetime.now().strftime("%H:%M %d.%m.%Y"))
#     h, w = color_map_prev.shape
    if color_map_actual.size != color_map_prev.size:
        print("WIND: Cloud maps in find_best_shift_of_binary_maps() have different sizes")
        return
    """
    import tools as tls
    binary_map_prev, _ = tls.convert_color_to_binary_map_and_filter(color_map_prev)
    binary_map_actual, _ = tls.convert_color_to_binary_map_and_filter(color_map_actual)
    rect_prev = tls.calc_cloud_rectangle(binary_map_prev)
    rect_actual = tls.calc_cloud_rectangle(binary_map_actual)
    v_shift_begin = rect_actual.bottom - (rect_prev.bottom - rect_prev.top)
    v_shift_end = rect_actual.top - rect_prev.top
    h_shift_begin = rect_actual.left - rect_prev.left
    h_shift_end = rect_actual.right - (rect_prev.right - rect_prev.left)
    
    min_v_shift = min(v_shift_begin, v_shift_end)
    max_v_shift = max(v_shift_begin, v_shift_end)
    min_h_shift = min(h_shift_begin, h_shift_end)
    max_h_shift = max(h_shift_begin, h_shift_end)
    """
    min_v_shift, max_v_shift, min_h_shift, max_h_shift = -80, 80, -80, 80
    result_type = c_int * 2
    result = result_type(0, 0) 
    compiled_lib.find_best_shift_between_color_maps.argtypes = [result_type,
            third_level_array_type, third_level_array_type, c_int, c_int, c_int, c_int]
    compiled_lib.find_best_shift_between_color_maps.restype = None
    
    c_color_map_prev = convert_pxl_to_c_color_map(color_map_prev.load())
    c_color_map_actual = convert_pxl_to_c_color_map(color_map_actual.load())
    
    compiled_lib.find_best_shift_between_color_maps(result, c_color_map_prev, 
            c_color_map_actual, min_h_shift, max_h_shift, min_v_shift, max_v_shift)
    
    
    return (result[0], result[1])