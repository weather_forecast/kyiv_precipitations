import time, datetime, urllib.request, os.path, shutil

def grab_snapshot(actual_snapshot_dt_str):
    url_path = "https://images.themeteocompany.com/image?text=false&output=png&time=" + \
    actual_snapshot_dt_str + "&type=radareuropawsg84&regio=euwide&w=578&h=560&sw=41|22&ne=44|25&z=geobound&v=null&server=ws120api.weerslag.nl&googlemaps=1"
    
#     url_path = "http://meteo.gov.ua/radars/Ukr_J " + actual_snapshot_dt_str + ".jpg"
    local_path = "../snapshots_meteox/meteox" + actual_snapshot_dt_str + ".png"
    
    if not os.path.isfile(local_path):
        with urllib.request.urlopen(url_path) as response, open(local_path, 'wb') as out_file:
            shutil.copyfileobj(response, out_file)
            return 1
            

if __name__ == "__main__":
    
    start_time = time.time()
    
    now_dt = datetime.datetime.utcnow()
    now_str = now_dt.strftime("%Y-%m-%d %H-%M")
    now_dt_round = datetime.datetime.strptime(now_str, "%Y-%m-%d %H-%M")
    now_minutes = int(now_str[-2:])
    
    snapshot_datetime = now_dt_round - datetime.timedelta(minutes=(15 + (now_minutes % 15)))
    
#     start_datetime = datetime.datetime.strptime("2016-12-14 19-19-00", 
#                                                 "%Y-%m-%d %H-%M-%S")
#     snapshot_datetime = start_datetime
    fails = 0
    while fails < 150:
        actual_snapshot_dt_str = snapshot_datetime.strftime("%Y%m%d%H%M")
        print (actual_snapshot_dt_str)
        if grab_snapshot(actual_snapshot_dt_str) == 1:
            print ("Succesful!")
        else:
            fails += 1
            print ("Could not download!")
        
        snapshot_datetime -= datetime.timedelta(minutes=15)
#         url_path = "http://meteo.gov.ua/radars/Ukr_J " + actual_snapshot_dt_str + ".jpg"
#         local_path = "../snapshots/Ukr_J " + actual_snapshot_dt_str + ".jpg"
#         if not os.path.isfile(local_path):
#             urllib.urlretrieve(url_path, local_path)
#             if os.path.getsize(local_path) < 190 * 1024:
#                 os.remove(local_path)
#                 fails += 1
#             else:
#                 print actual_snapshot_dt_str
#         else:
#             break
    
    print ("Downloading time: %.1f sec" % (time.time() - start_time))