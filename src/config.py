CLEAR_MAP_PATH = "selected_snapshots/free_snapshot.jpg"

SNAPSHOT_FOLDER = "snapshots"

SECONDS_IN_MINUTE = 60
MINUTES_IN_HOUR = 60
PIXELS_IN_KILOMETERS = 2
WIND_TIME_SHIFT_IN_HOURS = 0.5 # minutes

MAX_COLOR_CODE = 255
WHITE_COLOR_TUPLE = (255, 255, 255, 0)
GREY_COLOR_TUPLE = (238,238,238)
BLACK_COLOR_TUPLE = (0, 0, 0)

FORECAST_HOURS = 2  # number of hours to predict weather
WEATHER_FORECAST_IMAGE_WIDTH = 720
WEATHER_FORECAST_IMAGE_HEIGHT = 50
WEATHER_FORECAST_IMAGE_TEXT_WIDTH_PADDING = 30
WEATHER_FORECAST_IMAGE_TEXT_HEIGHT_PADDING = 25

WIDTH_STEP_BETWEEN_TEXT_ON_PC_IMAGE = 60

WEATHER_FORECAST_MOBILE_IMAGE_WIDTH = 720
WEATHER_FORECAST_MOBILE_IMAGE_HEIGHT = WEATHER_FORECAST_MOBILE_IMAGE_WIDTH

DELTA_TIME = 120 # in minutes
TEST_START_DATETIME = "22:39 24.12.2016"
# coordinates of the work
X_WORK = 342
Y_WORK = 280
# coordinates of the apartment
X_HOME = 363
Y_HOME = 272


# ALL Below is UNUSED
weak_precipitation = "0x08ff09"
moderate_precipitation = "0x00c101"
strong_precipitation = "0x008102" 
weak_rainfall = "0x00bfc5"
moderate_rainfall = "0x0500ff" 
strong_rainfall = "0x000081"

WEATHER_DICT = \
    {
        "precipitation":
        {
            "weak": (8, 255, 9),
            "moderate": (0, 193, 1),
            "strong": (0, 129, 2)
        },
        "rainfall":
        {
            "weak": (0, 191, 197),
            "moderate": (5, 0, 255),
            "strong": (0, 0, 129)
        }
    }