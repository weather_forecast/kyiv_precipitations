import time, datetime

from PIL import ImageFile

import src.config as cnf
import src.tools as tls

FORECAST_X_POINT = cnf.X_WORK
FORECAST_Y_POINT = cnf.Y_WORK

ImageFile.LOAD_TRUNCATED_IMAGES = True


def loop(mode="work"):
    if mode == "test":
        delta_time = datetime.timedelta(seconds=1)
        start_datetime = datetime.datetime.strptime(cnf.TEST_START_DATETIME, "%H:%M %d.%m.%Y")

    while True:
        '''!!!!!!! Need to replace warnings to other analysis script !!!!!!!'''
#         is_weather_warning_printed = False
        if mode == "work":
            start_datetime = datetime.datetime.utcnow()
        
        weather_image = tls.get_weather_map_for_datetime(start_datetime, ["save"])
        if weather_image is None:
            start_datetime += datetime.timedelta(minutes=10)
            if mode == "work":
                time.sleep(10 * cnf.SECONDS_IN_MINUTE)
            continue
            
        [last_wind_calculation_datetime, wind_x_shift, wind_y_shift] = tls.get_result_of_cloud_shift()
        if tls.check_wind_calculation_relevance(start_datetime,
                                                last_wind_calculation_datetime, 
                                                wind_x_shift, wind_y_shift):
            time_forecast = tls.make_forecast_for_point(weather_image, wind_x_shift, 
                                                        wind_y_shift, cnf.X_HOME, cnf.Y_HOME)
            tls.convert_time_forecast_to_image(time_forecast, start_datetime, [mode, "print"])
        else:
            print("FORECAST: Wind is absent or no precipitations are found on the map")
            # TODO draw not white pixels, but at pixels at x, y for the whole period of time
            time_forecast = tls.get_clear_forecast()
            tls.convert_time_forecast_to_image(time_forecast, start_datetime)
            if mode == "work":
                time.sleep(cnf.SECONDS_IN_MINUTE - datetime.datetime.now().second) #
                continue
        
        if mode == "work":
            time.sleep(cnf.SECONDS_IN_MINUTE - datetime.datetime.now().second)
        elif mode == "test":
            start_datetime += datetime.timedelta(minutes=10)
            # noinspection PyUnboundLocalVariable
            delta_time += datetime.timedelta(minutes=10)
            if delta_time > datetime.timedelta(minutes=40):
                break


if __name__ == "__main__":
    loop()
