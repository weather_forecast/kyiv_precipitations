#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define WIDTH 			767
#define HEIGHT			630
#define COLOR_NUMBER 	3
#define MAX_COLOR_CODE	255

void shift_color_map(uint8_t color_map[WIDTH][HEIGHT][COLOR_NUMBER],
		uint8_t shifted_map[WIDTH][HEIGHT][COLOR_NUMBER], int h_shift, int v_shift)
{
	int x, y, z;
	// init
	for (x = 0; x < WIDTH; x++)
	{
		for (y = 0; y < HEIGHT; y++)
		{
			for (z = 0; z < COLOR_NUMBER; z++)
			{
				shifted_map[x][y][z] = MAX_COLOR_CODE;
			}
		}
	}
	// shifting
	for (x = 0; x < WIDTH; x++)
	{
		for (y = 0; y < HEIGHT; y++)
		{
			if (!((color_map[x][y][0] == MAX_COLOR_CODE) &&
				  (color_map[x][y][1] == MAX_COLOR_CODE) &&
				  (color_map[x][y][2] == MAX_COLOR_CODE)
				 ) &&
				 ((x + h_shift < WIDTH) && (x + h_shift > 0)) &&
				 ((y + v_shift < HEIGHT) && (y + v_shift > 0))
			   )
			{
				for (z = 0; z < COLOR_NUMBER; z++)
				{
					shifted_map[x + h_shift][y + v_shift][z] = color_map[x][y][z];
				}
			}
		}
	}
}


uint64_t calc_color_correlation(uint8_t pxl_1[WIDTH][HEIGHT][COLOR_NUMBER],
		uint8_t pxl_2[WIDTH][HEIGHT][COLOR_NUMBER])
{
	uint64_t correlation = 0;
	int x, y, z;
	for (x = 0; x < WIDTH; x++)
	{
		for (y = 0; y < HEIGHT; y++)
		{
			for (z = 0; z < COLOR_NUMBER; z++)
			{
				correlation += abs(pxl_1[x][y][z] - pxl_2[x][y][z]);
			}
		}
	}
	return correlation;
}


void find_best_shift_between_color_maps(int best_shift[2],
		uint8_t color_map_prev[WIDTH][HEIGHT][COLOR_NUMBER],
		uint8_t color_map_actual[WIDTH][HEIGHT][COLOR_NUMBER],
		int min_h_shift, int max_h_shift, int min_v_shift, int max_v_shit)
{
	uint64_t min_corr = 0-1, corr;
	int x, y;
	uint8_t shifted_prev_map[WIDTH][HEIGHT][COLOR_NUMBER];
	for (x = min_h_shift; x < max_h_shift; x++)
	{
		for (y = min_v_shift; y < max_v_shit; y++)
		{
			shift_color_map(color_map_prev, shifted_prev_map, x, y);
			corr = calc_color_correlation(shifted_prev_map, color_map_actual);
			if (corr < min_corr)
			{
				min_corr = corr;
				best_shift[0] = x;
				best_shift[1] = y;
			}
		}
	}
//	printf("%llu\n", min_corr);
//	fflush(stdout);
}

