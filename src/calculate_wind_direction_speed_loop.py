import time
import datetime

import src.tools as tls
import src.compiled_tools as ctls
import src.config as cnf

from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True


def loop(mode="work"):
    is_no_clouds = False
    if mode == "test":
        start_datetime = datetime.datetime.strptime(cnf.TEST_START_DATETIME, "%H:%M %d.%m.%Y")
    else:
        start_datetime = None
    while True:
        is_no_clouds, start_datetime = one_iteration(mode, is_no_clouds, start_datetime)


def one_iteration(mode="work", is_no_clouds=False,
                  start_datetime=datetime.datetime.strptime(cnf.TEST_START_DATETIME, "%H:%M %d.%m.%Y")):
    if mode == "work":
        start_datetime = datetime.datetime.utcnow()

    # noinspection PyUnboundLocalVariable
    if not tls.is_it_time_to_start_calculation(start_datetime):
        time.sleep(60)
    elif is_no_clouds:
        time.sleep(10 * 60)
        is_no_clouds = False
    else:
        start_time = time.time()
        actual_weather_map = tls.get_weather_map_for_datetime(start_datetime, options=["save"])
        previous_datetime = start_datetime - datetime.timedelta(minutes=cnf.DELTA_TIME)
        previous_weather_map = tls.get_weather_map_for_datetime(previous_datetime, options=["save"])
        if actual_weather_map is not None and previous_weather_map is not None:
            if (not tls.check_is_weather_image_clear(actual_weather_map)) and \
                    (not tls.check_is_weather_image_clear(actual_weather_map)):
                x_shift, y_shift = \
                    ctls.compiled_find_best_shift_between_color_maps(
                        previous_weather_map, actual_weather_map, ["print"])

                tls.save_result_of_cloud_shift(start_datetime, x_shift,
                                               y_shift, time_shift=cnf.DELTA_TIME / 60.0,
                                               options=["print"])
                print("WIND: Properties calculation time: %.0f sec" % (time.time() - start_time))
                if mode == "test":
                    import make_weather_forecast_loop as mwfc
                    mwfc.loop()
            else:
                is_no_clouds = True
                tls.save_result_of_cloud_shift(start_datetime, None,
                                               None, ["print"])
        else:
            print("WIND: Error! Radar snapshots are not available")

    if mode == "test":
        start_datetime += datetime.timedelta(minutes=10)

    return is_no_clouds, start_datetime


if __name__ == "__main__":
    loop()
