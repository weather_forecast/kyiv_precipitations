from threading import Thread

import src.calculate_wind_direction_speed_loop as wind_calculation
import src.make_weather_forecast_loop as precipitation_forecast
import website.index as website_index


if __name__ == "__main__":
    #wind_calculation.one_iteration()

    wind_calculation_thread = Thread(target=wind_calculation.loop)
    forecast_thread = Thread(target=precipitation_forecast.loop)
    site_thread = Thread(target=website_index.loop)

    wind_calculation_thread.start()
    forecast_thread.start()
    site_thread.start()

    # TODO check acquire/lock for wind_shift reading/writing and other shared resources
    # TODO close figures/images to free memory
    wind_calculation_thread.join()
    forecast_thread.join()
    site_thread.join()

    print("Main process is finished")
